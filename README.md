# Application annuaire de recherche Dockerisé 

Préréquis : avoir docker d'installer sur votre machine

Pour installer et déployer l'application :

  - Clonner le projet à l'aide de la commande git clone
  - à la racine du projet (là où se trouve le fichier docker-compose.yml), lancer la commande :
            docker-compose up -d --remove-orphans --build
        ==> Docker se chargera de compiler et de déployer les deux micro-services pour vous
		
  - Accéder à l'application sur le lien : http://localhost:9090/annuaire-ui/#/
  
  NB : quelques captures de l'application en fonctionnement se trouvent dans le dossier 'images' 